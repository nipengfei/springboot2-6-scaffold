package com.snk.kof.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

/**
 * @author keshan
 * @date 2022-01-02 下午3:13
 */
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum CommonResultEnum implements ResultCode {

    /**
     * 请求成功
     */
    SUCCESS(200, "请求成功"),

    /**
     * 资源不存在
     */
    NO_HANDLER_FOUND(404, "资源不存在"),

    /**
     * 请求方式不被允许
     */
    HTTP_METHOD_NOT_ALLOWED(493, "请求方式不被允许"),

    /**
     * media type不被支持
     */
    HTTP_MEDIA_TYPE_NOT_SUPPORTED(494, "media type不被支持"),

    /**
     * json解析错误
     */
    JSON_PARSE_ERROR(495, "json解析错误"),

    /**
     * 限流
     */
    RATE_LIMIT(496, "触发限流"),

    /**
     * 参数校验失败
     */
    ERROR_PARAM_VALIDATOR(497, "参数校验失败"),

    /**
     * 请求失败
     */
    FAIL(500, "请求失败"),

    /**
     * 系统异常
     */
    SYSTEM_ERROR(501, "系统异常,请重试!"),

    /**
     * 业务异常
     */
    BUSINESS_ERROR(502, "业务异常"),
    ;

    int code;

    String message;

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
