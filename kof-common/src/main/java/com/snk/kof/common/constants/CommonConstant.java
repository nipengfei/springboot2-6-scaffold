package com.snk.kof.common.constants;

/**
 * @author keshan
 * @date 2022-01-02 下午3:34
 */
public class CommonConstant {

    /**
     * 分页参数 当前页
     */
    public static final long pageNum = 1L;

    /**
     * 分页参数 每页数据数
     */
    public static final long pageSize = 10L;

}
