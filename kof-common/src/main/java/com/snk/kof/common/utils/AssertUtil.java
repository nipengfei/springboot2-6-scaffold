package com.snk.kof.common.utils;

import com.snk.kof.common.exception.BusinessException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

/**
 * @author keshan
 * @date 2022-01-02 下午8:46
 */
public class AssertUtil {

    public static void isTrue(Boolean expression, String message) {
        notNull(expression, "表达式");
        if (!expression) {
            throw new BusinessException(message);
        }
    }

    public static void notNull(Object data, String property) {
        if (Objects.isNull(data)) {
            throw new BusinessException(property + "不能为null");
        }
    }

    public static void notBlank(String data, String property) {
        if (StringUtils.isBlank(data)) {
            throw new BusinessException(property + "不能为空字符串");
        }
    }

    public static <T> void notEmpty(T[] array, String property) {
        if (ArrayUtils.isEmpty(array)) {
            throw new BusinessException(property + "不能为空数组");
        }
    }

    public static <T> void notEmpty(Collection<T> collection, String property) {
        if (CollectionUtils.isEmpty(collection)) {
            throw new BusinessException(property + "不能为空集合");
        }
    }

    public static <T> void notEmpty(Map<T, T> map, String property) {
        if (MapUtils.isEmpty(map)) {
            throw new BusinessException(property + "不能为空Map");
        }
    }

}
