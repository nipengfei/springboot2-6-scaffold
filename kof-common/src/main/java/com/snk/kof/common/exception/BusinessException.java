package com.snk.kof.common.exception;

import com.snk.kof.common.enums.ResultCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author keshan
 * @date 2022-01-02 下午3:44
 */
@Setter
@Getter
@Accessors(chain = true)
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = -200441320773992531L;

    int code;

    String message;

    public BusinessException() {
        super();
    }

    public BusinessException(ResultCode resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
    }

    public BusinessException(String message) {
        this.message = message;
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
