package com.snk.kof.common.enums;

/**
 * @author keshan
 * @date 2022-01-02 下午3:16
 */
public interface ResultCode {

    int getCode();

    String getMessage();

}
