package com.snk.kof.component.validator.group;

import javax.validation.GroupSequence;

/**
 * @author keshan
 * @date 2022-01-02 下午7:23
 */
@GroupSequence({InsertGroup.class, UpdateGroup.class})
public interface insertAndUpdateGroup {

}
