package com.snk.kof.component.jackson.annotation;

import com.snk.kof.component.jackson.config.JacksonSerializerConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author keshan
 * @date 2022-01-03 上午11:08
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(JacksonSerializerConfig.class)
public @interface EnableJacksonSerializer {
}
