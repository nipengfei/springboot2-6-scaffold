package com.snk.kof.mbp.base;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author keshan
 * @date 2022-01-03 下午4:23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@FieldNameConstants(level = AccessLevel.PUBLIC)
public class AbstractDO implements Serializable {

    private static final long serialVersionUID = -2791454961427974525L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    Long id;

    /**
     * 逻辑删标识 0正常 1删除
     */
    @TableField(fill = FieldFill.INSERT)
    @TableLogic
    Integer isDelete;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    LocalDateTime updateTime;

}
