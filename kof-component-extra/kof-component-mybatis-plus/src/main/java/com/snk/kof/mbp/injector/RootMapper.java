package com.snk.kof.mbp.injector;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author keshan
 * @date 2022-01-03 下午5:20
 */
public interface RootMapper<T> extends BaseMapper<T> {

    /**
     * 批量新增
     *
     * @param list 数据集
     * @return 批量新增成功记录数
     */
    int insertBatch(@Param("list") List<T> list);

    /**
     * 批量更新
     *
     * @param list 数据集
     * @return 批量更新成功记录数
     */
    int updateBatch(@Param("list") List<T> list);

}
