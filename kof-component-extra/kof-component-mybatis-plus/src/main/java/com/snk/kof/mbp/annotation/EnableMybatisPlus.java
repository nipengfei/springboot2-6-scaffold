package com.snk.kof.mbp.annotation;

import com.snk.kof.mbp.config.MybatisPlusConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author keshan
 * @date 2022-01-03 下午4:42
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(MybatisPlusConfig.class)
public @interface EnableMybatisPlus {



}
