package com.snk.kof.mbp.base;

import cn.hutool.core.util.PageUtil;
import com.snk.kof.common.constants.CommonConstant;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author keshan
 * @date 2022-01-03 下午4:47
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AbstractQueryParam implements Serializable {

    private static final long serialVersionUID = -7903195091633920663L;

    /**
     * 分页数据 当前页
     */
    long pageNum = CommonConstant.pageNum;

    /**
     * 分页数据 每页数据数
     */
    long pageSize = CommonConstant.pageSize;

    /**
     * 查询开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime queryStartTime;

    /**
     * 查询结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime queryEndTime;

}
