package com.snk.kof.extra.quartz.annotation;

import com.snk.kof.extra.quartz.registrar.QuartzCronScheduledRegistrar;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;


/**
 * @author keshan
 * @date 2022-01-03 上午9:58
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
@Import(QuartzCronScheduledRegistrar.class)
public @interface QuartzCronScheduled {

    String cronExpression();

    String jobName();

    String jobGroup() default "default";

    String description() default "";

    int priority() default 5;

    boolean durability() default true;

    boolean requestsRecovery() default true;

    boolean isNonConcurrent() default true;

    boolean isUpdateData() default true;
}
