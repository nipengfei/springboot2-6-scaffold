package com.snk.kof.extra.quartz.annotation;

import com.snk.kof.extra.quartz.registrar.EnableQuartzCronScheduledRegistrar;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author keshan
 * @date 2022-01-03 上午9:58
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(EnableQuartzCronScheduledRegistrar.class)
public @interface EnableQuartzCronScheduled {

}
