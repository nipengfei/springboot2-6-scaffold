docker stop entrust-server
docker rm entrust-server
docker rmi entrust-server
docker build -t entrust-server .
docker run -p 9090:9090 --name entrust-server --restart=always --privileged=true -v /entrust/entrust-server/logs:/home/web/logs -d entrust-server:latest
tail -100f /entrust/entrust-server/logs/info.log
