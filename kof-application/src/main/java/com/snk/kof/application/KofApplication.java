package com.snk.kof.application;

import cn.hutool.extra.spring.EnableSpringUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author keshan
 * @date 2022-01-02 下午2:51
 */
@EnableSpringUtil
@MapperScan(basePackages = {"com.snk.kof.**.mapper"})
@SpringBootApplication(scanBasePackages = {"com.snk.kof"})
public class KofApplication {
    public static void main(String[] args) {
        SpringApplication.run(KofApplication.class);
    }
}
