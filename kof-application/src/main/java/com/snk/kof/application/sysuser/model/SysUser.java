package com.snk.kof.application.sysuser.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.snk.kof.mbp.base.AbstractDO;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

/**
 * @author keshan
 * @date 2022-01-03 下午5:53
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SysUser extends AbstractDO {

    private static final long serialVersionUID = 6360886652561794079L;

    @TableField(value = "`username`")
    String username;

    @TableField(value = "`password`")
    String password;

}
