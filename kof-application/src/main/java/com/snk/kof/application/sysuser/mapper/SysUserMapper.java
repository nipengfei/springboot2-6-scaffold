package com.snk.kof.application.sysuser.mapper;

import com.snk.kof.application.sysuser.model.SysUser;
import com.snk.kof.mbp.injector.RootMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author keshan
 * @date 2022-01-03 下午5:55
 */
@Mapper
public interface SysUserMapper extends RootMapper<SysUser> {
}
