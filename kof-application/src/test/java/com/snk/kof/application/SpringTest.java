package com.snk.kof.application;

import com.google.common.collect.Lists;
import com.snk.kof.application.sysuser.mapper.SysUserMapper;
import com.snk.kof.application.sysuser.model.SysUser;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author keshan
 * @date 2022-01-03 下午6:00
 */
@Slf4j
@SpringBootTest
class SpringTest {

    @Autowired
    SysUserMapper sysUserMapper;


    @Test
    void t3() {
        SysUser sysUser1 = new SysUser().setPassword("update1");
        sysUser1.setId(1L);
        SysUser sysUser2 = new SysUser().setPassword("update2");
        sysUser2.setId(2L);
        SysUser sysUser3 = new SysUser().setPassword("update3");
        sysUser3.setId(3L);
        List<SysUser> userList = Lists.newArrayList(sysUser1, sysUser2, sysUser3);

        sysUserMapper.updateBatch(userList);

    }

    @Test
    void t2() {
        sysUserMapper.insertBatch(
                Lists.newArrayList(
                        new SysUser().setUsername("name1").setPassword("pass1"),
                        new SysUser().setUsername("name2").setPassword("pass2"),
                        new SysUser().setUsername("name3").setPassword("pass3")
                )
        );
    }

    @Test
    void t1() {
        sysUserMapper.insert(new SysUser().setUsername("username").setPassword("password"));
    }

}
